## [1.2.0](https://gitlab.com/place-me/external-providers/compare/v1.1.0...v1.2.0) (2021-11-23)


### Features

* **net:** update to .NET 6.0 ([1a5b29e](https://gitlab.com/place-me/external-providers/commit/1a5b29ed1212c5a000a10ce149ac840b5214eb98))


### Cleanup

* **deps:** update dependency microsoft.identity.client to v4.37.0 ([7783a9d](https://gitlab.com/place-me/external-providers/commit/7783a9d7f88fefb971a3d9c89527dd919a21b960))
* **deps:** update dependency microsoft.identity.client to v4.38.0 ([84670d2](https://gitlab.com/place-me/external-providers/commit/84670d2c240e42513fe9919db6a6d6ebfadafaa8))
* **deps:** update dependency microsoft.identitymodel.jsonwebtokens to v6.14.0 ([aa56382](https://gitlab.com/place-me/external-providers/commit/aa5638271f998ff21f202990cd9fbb9095c10f81))
* **deps:** update dependency microsoft.identitymodel.jsonwebtokens to v6.14.1 ([b924fc6](https://gitlab.com/place-me/external-providers/commit/b924fc6353c3572e9ae78275542f63317fcb7744))
* **deps:** update dependency microsoft.net.test.sdk to v17 ([f6cac84](https://gitlab.com/place-me/external-providers/commit/f6cac845723cc34bca4c9ff02ee7f0964e094c73))
* **deps:** update dependency microsoft.sourcelink.gitlab to v1.1.1 ([263f6ee](https://gitlab.com/place-me/external-providers/commit/263f6eedd1326cbeb46244c7603c6fcc2daf9bfc))
* **deps:** update dependency nunit3testadapter to v4.1.0 ([04be3b8](https://gitlab.com/place-me/external-providers/commit/04be3b847599984c37c1abfbfd1582f26bcebb2f))
