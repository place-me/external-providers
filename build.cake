#addin nuget:?package=Cake.SemVer&version=4.0.0
#addin nuget:?package=semver&version=2.2.0

///////////////////////////////////////////////////////////////////////////////
// ARGUMENTS
///////////////////////////////////////////////////////////////////////////////

var target = Argument<string>("target", "Default");
var configuration = Argument<string>("configuration", "Release");
var versionNumber = Argument<string>("versionNumber", "0.0.0");

///////////////////////////////////////////////////////////////////////////////
// VARIABLES
///////////////////////////////////////////////////////////////////////////////

var solution = GetFiles("./*.sln").First();
var packageProject = GetFiles("./src/Placeme.Providers/*.csproj").First();
var artifactsDir = Directory("./dist");

///////////////////////////////////////////////////////////////////////////////
// SETUP / TEARDOWN
///////////////////////////////////////////////////////////////////////////////

Setup(_ =>
{
    Information("Target {0}", target);
    Information("Target {0}", target);
    Information("Configuration {0}", configuration);
    Information("Version {0}", versionNumber);
});

Teardown(_ =>
{
    Information("Finished running tasks");
});

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Build")
    ;

Task("Build")
    .IsDependentOn("Run_dotnet_info")
    .IsDependentOn("Clean")
    .IsDependentOn("Build_solution")
    .IsDependentOn("Run_tests")
    .IsDependentOn("Package")
    ;

Task("Run_dotnet_info")
    .Does(() =>
{
    Information("dotnet --info");
    StartProcess("dotnet", new ProcessSettings { Arguments = "--info" });
});

Task("Clean")
    .Does(_ =>
{
    Information("Cleaning {0}, bin and obj folders", artifactsDir);

    CleanDirectory(artifactsDir);
    CleanDirectories("./src/**/bin");
    CleanDirectories("./src/**/obj");
});

Task("Build_solution")
    .Does(_ => {
		Information("Building solution {0} v{1}", solution.GetFilenameWithoutExtension(), versionNumber);

        var parsedVersion = ParseSemVer(versionNumber);
        Information(parsedVersion.ToString());
		DotNetBuild(solution.FullPath, new DotNetBuildSettings()
		{
			Configuration = configuration,
			MSBuildSettings = new DotNetCoreMSBuildSettings()
				.SetVersion(parsedVersion.ToString())
                .SetInformationalVersion(parsedVersion.ToString())
				.SetFileVersion(parsedVersion.ToString())
				// Only including a major version in the AssemblyVersion to reduce binding redirects
				// https://docs.microsoft.com/en-us/dotnet/standard/library-guidance/versioning#assembly-version
				.WithProperty("AssemblyVersion", $"{parsedVersion.Major}.0.0")
				// 0 = use as many processes as there are available CPUs to build the project
				// see: https://develop.cakebuild.net/api/Cake.Common.Tools.MSBuild/MSBuildSettings/60E763EA
				.SetMaxCpuCount(0)
		});
	});

Task("Run_tests")
    .Does(_ =>
{
    Information("Testing solution {0}", solution.GetFilenameWithoutExtension());

    DotNetCoreTest(solution.FullPath, new DotNetCoreTestSettings
    {
        Configuration = configuration,
        ResultsDirectory = artifactsDir,
        Loggers = new[] { "junit;LogFilePath=..\\..\\artifacts\\{assembly}-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose" },
		TestAdapterPath = new DirectoryPath("."),
        NoBuild = true,
        NoRestore = true
    });
});

Task("Package")
    .Does(_ =>
{
    Information("Packaging solution {0}", packageProject.GetFilenameWithoutExtension());

    var parsedVersion = ParseSemVer(versionNumber);

    DotNetCorePack(packageProject.FullPath, new DotNetCorePackSettings {
        Configuration = configuration,
        OutputDirectory = artifactsDir,
        NoBuild = true,
        NoRestore = true,
        MSBuildSettings = new DotNetCoreMSBuildSettings()
			.SetInformationalVersion(parsedVersion.ToString())
			.SetFileVersion(parsedVersion.ToString())
			.SetVersion(parsedVersion.ToString())
			.WithProperty("ContinuousIntegrationBuild", "true")
    });
});

///////////////////////////////////////////////////////////////////////////////
// EXECUTION
///////////////////////////////////////////////////////////////////////////////

RunTarget(target);