using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

using NUnit.Framework;

using Placeme.Providers.Azure;
using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Test.Azure;

[TestFixture]
public class SerializationTests
{
	private IRemoteUser _singleUser = new AzureUser();
	private ICollection<IRemoteUser>? _userList;

	[OneTimeSetUp]
	public void Setup()
	{
		_singleUser = new AzureUser
		{
			City = "Bern",
			Country = "Switzerland",
			Department = "IT",
			Division = "IT Sec",
			Email = "azure@user.ch",
			Firstname = "Azure",
			Function = "Consulting",
			Id = Guid.NewGuid().ToString(),
			Lastname = "User",
			PhoneNumber = "076 969 69 69",
			Street = "Bernstrasse 69",
			ZipCode = "3008",
			Groups = new List<AzureUserGroup>
			{
				new()
				{
					Id = Guid.NewGuid().ToString(),
					Description = "Some group",
					Name = "THAT Group"
				}
			}
		};

		_userList = new List<IRemoteUser>
		{
			_singleUser,
			_singleUser
		};
	}

	[Test]
	public void ShouldSerializeSingleRemoteUser()
	{
		var serialized = JsonSerializer.Serialize(_singleUser);
		Assert.IsNotEmpty(serialized);
	}

	[Test]
	public void ShouldDeserializeSingleRemoteUser()
	{
		var jsonOptions = new JsonSerializerOptions
		{
			AllowTrailingCommas = true
		};

		var serialized = JsonSerializer.Serialize(_singleUser, typeof(AzureUser), jsonOptions);
		Assert.IsNotEmpty(serialized);
		var deserialized = JsonSerializer.Deserialize(serialized, typeof(AzureUser), jsonOptions) as IRemoteUser;
		Assert.IsNotNull(deserialized);
		Assert.AreEqual(_singleUser.Id, deserialized!.Id);
	}

	[Test]
	public void ShouldSerializeRemoteUserList()
	{
		var serialized = JsonSerializer.Serialize(_userList);
		Assert.IsNotEmpty(serialized);
	}

	[Test]
	public void ShouldDeserializeRemoteUserList()
	{
		var jsonOptions = new JsonSerializerOptions
		{
			AllowTrailingCommas = true
		};

		var serialized = JsonSerializer.Serialize(_userList, jsonOptions);
		Assert.IsNotEmpty(serialized);
		var deserialized = JsonSerializer.Deserialize(serialized, typeof(List<AzureUser>), jsonOptions) as IEnumerable<IRemoteUser>;
		Assert.IsNotNull(deserialized);
		Assert.AreEqual(_userList!.Count, deserialized!.Count());
	}
}