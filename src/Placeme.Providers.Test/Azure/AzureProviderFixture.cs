using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Graph;

using Moq;

using NUnit.Framework;

using Placeme.Providers.Azure;
using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Test.Azure;

[TestFixture]
public class AzureProviderFixture
{
	private readonly ICollection<Place> _testPlaces = new Collection<Place>();
	private readonly ICollection<User> _testUsers = new Collection<User>();
	private IRemoteProvider? _provider;

	private GraphServiceUsersCollectionResponse UsersMockResponse()
	{
		var res = new GraphServiceUsersCollectionResponse
		{
			Value = new GraphServiceUsersCollectionPage()
		};
		_testUsers.ToList().ForEach(u => res.Value.Add(u));
		return res;
	}

	private GraphServicePlacesCollectionResponse PlacesMockResponse()
	{
		var placesCollection = new GraphServicePlacesCollectionPage();
		foreach (var place in _testPlaces)
		{
			placesCollection.Add(place);
		}
		return new GraphServicePlacesCollectionResponse { Value = placesCollection };
	}

	[SetUp]
	public Task SetUpFixture()
	{
		var authMock = new Mock<IAuthenticationProvider>();
		authMock.Setup(auth => auth.AuthenticateRequestAsync(It.IsAny<HttpRequestMessage>())).Returns(Task.CompletedTask);

		var httpMock = new Mock<IHttpProvider>();
		httpMock.Setup(h => h.Serializer).Returns(new Serializer());
		httpMock.Setup(h =>
				h.SendAsync(
					It.Is<HttpRequestMessage>(req => req!.RequestUri!.ToString().StartsWith("https://graph.microsoft.com/beta/users")),
					It.IsAny<HttpCompletionOption>(),
					It.IsAny<CancellationToken>()
				))
			.ReturnsAsync(() =>
			{
				var content = JsonContent.Create(UsersMockResponse());
				return new HttpResponseMessage(HttpStatusCode.OK) { Content = content };
			});

		httpMock.Setup(h =>
				h.SendAsync(
					It.Is<HttpRequestMessage>(req => req!.RequestUri!.ToString().StartsWith("https://graph.microsoft.com/beta/places")),
					It.IsAny<HttpCompletionOption>(),
					It.IsAny<CancellationToken>()))
			.ReturnsAsync(() =>
			{
				var content = JsonContent.Create(PlacesMockResponse());
				return new HttpResponseMessage(HttpStatusCode.OK) { Content = content };
			});

		var graphClient = new GraphServiceClient(authMock.Object, httpMock.Object);
		_provider = new AzureProvider(graphClient);

		return Task.CompletedTask;
	}

	[Test]
	public async Task GetAllResources()
	{
		var places = await _provider!.GetAssetsAsync();
		Assert.IsTrue(places.All(p => !string.IsNullOrWhiteSpace(p.Id)));
	}

	[Test]
	[Ignore("TODO")]
	public async Task ShouldIgnorePlacesInUserRequest()
	{
		var license = new AssignedLicense { SkuId = Guid.NewGuid() };
		_testUsers.Add(new User
		{
			Mail = "user@foobar.ch",
			AssignedLicenses = new List<AssignedLicense> { license }
		});
		_testUsers.Add(new User { Mail = "resource@foobar.ch" });

		var room = new Room { AdditionalData = new Dictionary<string, object>() };
		room.AdditionalData.Add("emailAddress", "resource@foobar.ch");
		_testPlaces.Add(room);

		var users = await _provider!.GetUsersAsync();
		var places = await _provider!.GetAssetsAsync();

		Assert.IsTrue(users.All(u => places.All(p => p.Email != u.Email)));
	}

	[Test]
	[Ignore("TODO")]
	public async Task AdditionalDataCanBeExtracted()
	{
		var room = new Room
		{
			DisplayName = "Everest",
			EmailAddress = "everest@mount.ch",
			FloorNumber = 69,
			IsWheelChairAccessible = true
		};
		_testPlaces.Add(room);

		var places = await _provider!.GetAssetsAsync();

		Assert.That(places.Count(), Is.EqualTo(1));
	}
}