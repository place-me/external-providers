using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Azure;

public class AzurePermission : IRemotePermission
{
	public string? Identifier { get; set; }
	public string? Description { get; set; }
	public bool Granted { get; set; }
}