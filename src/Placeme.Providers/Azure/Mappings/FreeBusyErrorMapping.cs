using Microsoft.Graph;

using Placeme.Providers.Models;

namespace Placeme.Providers.Azure.Mappings;

public static class FreeBusyErrorMapping
{
	public static RemoteFreeBusyError? Map(FreeBusyError? freeBusyError)
	{
		if (freeBusyError == null)
		{
			return null;
		}

		return new RemoteFreeBusyError
		{
			Message = freeBusyError.Message,
			ResponseCode = freeBusyError.ResponseCode
		};
	}
}