using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Graph;

using Placeme.Providers.Models;

namespace Placeme.Providers.Azure.Mappings;

public static class RecurrenceMapping
{
	public static PatternedRecurrence? Map(RemoteRecurrencePattern? rrp, DateTime startDate)
	{
		if (rrp == null)
		{
			return null;
		}

		var utcStartDate = startDate.ToUniversalTime();

		return new PatternedRecurrence
		{
			Pattern = new RecurrencePattern
			{
				DayOfMonth = rrp.DayOfMonth,
				DaysOfWeek = rrp.DaysOfWeek.Select(wd => Map(wd)),
				FirstDayOfWeek = Microsoft.Graph.DayOfWeek.Monday,
				Index = Map(rrp.Index),
				Interval = rrp.Interval,
				Month = rrp.Month,
				Type = Map(rrp.Type)
			},
			Range = new RecurrenceRange
			{
				StartDate = new Date(utcStartDate.Year, utcStartDate.Month, utcStartDate.Day),
				EndDate = rrp.Range.EndDate != null ? new Date(rrp.Range.EndDate.Value.Year, rrp.Range.EndDate.Value.Month, rrp.Range.EndDate.Value.Day) : null,
				NumberOfOccurrences = rrp.Range.NumberOfOccurrences,
				Type = Map(rrp.Range.Type),
				RecurrenceTimeZone = TimeZoneInfo.Utc.Id
			}
		};
	}

	public static RemoteRecurrencePattern? Map(PatternedRecurrence? pr)
	{
		if (pr == null)
		{
			return null;
		}

		return new RemoteRecurrencePattern
		{
			DayOfMonth = pr.Pattern.DayOfMonth ?? 0,
			DaysOfWeek = pr.Pattern.DaysOfWeek?.Select(dayOfWeek => Map(dayOfWeek)) ?? new List<RemoteRecurrenceWeekDay>(),
			Index = Map(pr.Pattern.Index),
			Interval = pr.Pattern.Interval,
			Month = pr.Pattern.Month,
			Type = Map(pr.Pattern.Type),
			Range = new RemoteRecurrenceRange
			{
				Type = Map(pr.Range.Type),
				NumberOfOccurrences = pr.Range.NumberOfOccurrences,
				EndDate = pr.Range.EndDate != null ? new DateTime(pr.Range.EndDate.Year, pr.Range.EndDate.Month, pr.Range.EndDate.Day) : null
			}
		};
	}

	private static Microsoft.Graph.DayOfWeek Map(RemoteRecurrenceWeekDay weekDay)
	{
		return weekDay switch
		{
			RemoteRecurrenceWeekDay.Monday => Microsoft.Graph.DayOfWeek.Monday,
			RemoteRecurrenceWeekDay.Tuesday => Microsoft.Graph.DayOfWeek.Tuesday,
			RemoteRecurrenceWeekDay.Wednesday => Microsoft.Graph.DayOfWeek.Wednesday,
			RemoteRecurrenceWeekDay.Thursday => Microsoft.Graph.DayOfWeek.Thursday,
			RemoteRecurrenceWeekDay.Friday => Microsoft.Graph.DayOfWeek.Friday,
			RemoteRecurrenceWeekDay.Saturday => Microsoft.Graph.DayOfWeek.Saturday,
			RemoteRecurrenceWeekDay.Sunday => Microsoft.Graph.DayOfWeek.Sunday,
			_ => throw new ArgumentOutOfRangeException(nameof(weekDay)),
		};
	}

	private static RemoteRecurrenceWeekDay Map(Microsoft.Graph.DayOfWeek weekDay)
	{
		return weekDay switch
		{
			Microsoft.Graph.DayOfWeek.Monday => RemoteRecurrenceWeekDay.Monday,
			Microsoft.Graph.DayOfWeek.Tuesday => RemoteRecurrenceWeekDay.Tuesday,
			Microsoft.Graph.DayOfWeek.Wednesday => RemoteRecurrenceWeekDay.Wednesday,
			Microsoft.Graph.DayOfWeek.Thursday => RemoteRecurrenceWeekDay.Thursday,
			Microsoft.Graph.DayOfWeek.Friday => RemoteRecurrenceWeekDay.Friday,
			Microsoft.Graph.DayOfWeek.Saturday => RemoteRecurrenceWeekDay.Saturday,
			Microsoft.Graph.DayOfWeek.Sunday => RemoteRecurrenceWeekDay.Sunday,
			_ => throw new ArgumentOutOfRangeException(nameof(weekDay)),
		};
	}

	private static WeekIndex? Map(RemoteRecurrenceWeekIndex? weekIndex)
	{
		if (weekIndex == null)
		{
			return null;
		}

		return weekIndex switch
		{
			RemoteRecurrenceWeekIndex.First => WeekIndex.First,
			RemoteRecurrenceWeekIndex.Second => WeekIndex.Second,
			RemoteRecurrenceWeekIndex.Third => WeekIndex.Third,
			RemoteRecurrenceWeekIndex.Fourth => WeekIndex.Fourth,
			RemoteRecurrenceWeekIndex.Last => WeekIndex.Last,
			_ => throw new ArgumentOutOfRangeException(nameof(weekIndex)),
		};
	}

	private static RemoteRecurrenceWeekIndex? Map(WeekIndex? weekIndex)
	{
		if (weekIndex == null)
		{
			return null;
		}

		return weekIndex switch
		{
			WeekIndex.First => RemoteRecurrenceWeekIndex.First,
			WeekIndex.Second => RemoteRecurrenceWeekIndex.Second,
			WeekIndex.Third => RemoteRecurrenceWeekIndex.Third,
			WeekIndex.Fourth => RemoteRecurrenceWeekIndex.Fourth,
			WeekIndex.Last => RemoteRecurrenceWeekIndex.Last,
			_ => throw new ArgumentOutOfRangeException(nameof(weekIndex)),
		};
	}

	private static RecurrencePatternType? Map(RemoteRecurrenceType? recurrenceType)
	{
		if (recurrenceType == null)
		{
			return null;
		}

		return recurrenceType switch
		{
			RemoteRecurrenceType.Daily => RecurrencePatternType.Daily,
			RemoteRecurrenceType.Weekly => RecurrencePatternType.Weekly,
			RemoteRecurrenceType.AbsoluteMonthly => RecurrencePatternType.AbsoluteMonthly,
			RemoteRecurrenceType.RelativeMonthly => RecurrencePatternType.RelativeMonthly,
			RemoteRecurrenceType.AbsoluteYearly => RecurrencePatternType.AbsoluteYearly,
			RemoteRecurrenceType.RelativeYearly => RecurrencePatternType.RelativeYearly,
			_ => throw new ArgumentOutOfRangeException(nameof(recurrenceType)),
		};
	}

	private static RemoteRecurrenceType? Map(RecurrencePatternType? recurrenceType)
	{
		if (recurrenceType == null)
		{
			return null;
		}

		return recurrenceType switch
		{
			RecurrencePatternType.Daily => RemoteRecurrenceType.Daily,
			RecurrencePatternType.Weekly => RemoteRecurrenceType.Weekly,
			RecurrencePatternType.AbsoluteMonthly => RemoteRecurrenceType.AbsoluteMonthly,
			RecurrencePatternType.RelativeMonthly => RemoteRecurrenceType.RelativeMonthly,
			RecurrencePatternType.AbsoluteYearly => RemoteRecurrenceType.AbsoluteYearly,
			RecurrencePatternType.RelativeYearly => RemoteRecurrenceType.RelativeYearly,
			_ => throw new ArgumentOutOfRangeException(nameof(recurrenceType)),
		};
	}

	private static RecurrenceRangeType? Map(RemoteRecurrenceRangeType? recurrenceRangeType)
	{
		if (recurrenceRangeType == null)
		{
			return null;
		}

		return recurrenceRangeType switch
		{
			RemoteRecurrenceRangeType.EndDate => RecurrenceRangeType.EndDate,
			RemoteRecurrenceRangeType.NoEnd => RecurrenceRangeType.NoEnd,
			RemoteRecurrenceRangeType.Numbered => RecurrenceRangeType.Numbered,
			_ => throw new ArgumentOutOfRangeException(nameof(recurrenceRangeType)),
		};
	}

	private static RemoteRecurrenceRangeType? Map(RecurrenceRangeType? recurrenceRangeType)
	{
		if (recurrenceRangeType == null)
		{
			return null;
		}

		return recurrenceRangeType switch
		{
			RecurrenceRangeType.EndDate => RemoteRecurrenceRangeType.EndDate,
			RecurrenceRangeType.NoEnd => RemoteRecurrenceRangeType.NoEnd,
			RecurrenceRangeType.Numbered => RemoteRecurrenceRangeType.Numbered,
			_ => throw new ArgumentOutOfRangeException(nameof(recurrenceRangeType)),
		};
	}
}