using System.Linq;

using Microsoft.Graph;

using Placeme.Providers.Models;

namespace Placeme.Providers.Azure.Mappings;

public static class ScheduleInformationMapping
{
	public static RemoteScheduleInformation Map(ScheduleInformation si)
	{
		var remoteScheduleInfo = new RemoteScheduleInformation
		{
			AvailabilityView = si.AvailabilityView,
			Error = FreeBusyErrorMapping.Map(si.Error),
			ScheduleId = si.ScheduleId
		};

		si.ScheduleItems?.ToList().ForEach(i => remoteScheduleInfo.ScheduleItems.Add(ScheduleItemMapping.Map(i)));

		return remoteScheduleInfo;
	}
}