
using System;

using Microsoft.Graph;

using Placeme.Providers.Azure.Helpers;
using Placeme.Providers.Models;

namespace Placeme.Providers.Azure.Mappings;

public static class PlaceMapping
{
	public static RemoteAsset Map(Place place)
	{
		var room = ConvertPlaceToRoom(place);
		var azureResource = new RemoteAsset
		{
			AudioDeviceName = room.AudioDeviceName,
			BookingType = room.BookingType.HasValue ? room.BookingType.ToString() : string.Empty,
			Building = room.Building,
			Capacity = room.Capacity,
			DisplayDeviceName = room.DisplayDeviceName,
			Email = room.EmailAddress,
			FloorLabel = room.FloorLabel,
			FloorNumber = room.FloorNumber,
			Id = place.Id,
			IsWheelChairAccessible = EntityExtentions.ExtractFromAdditionalData<bool?>(place, "isWheelChairAccessible"),
			Label = room.Label,
			Name = room.DisplayName,
			Nickname = room.Nickname,
			Phone = room.Phone,
			VideoDeviceName = room.VideoDeviceName,
			Upn = room.EmailAddress
		};

		if (room.Address != null)
		{
			azureResource.City = room.Address.City;
			azureResource.Country = room.Address.CountryOrRegion;
			azureResource.Street = room.Address.Street;
			azureResource.ZipCode = room.Address.PostalCode;
		}

		if (room.GeoCoordinates != null)
		{
			azureResource.GeoCoordinatesAltitude = room.GeoCoordinates.Altitude;
			azureResource.GeoCoordinatesLatitude = room.GeoCoordinates.Latitude;
			azureResource.GeoCoordinatesLongitude = room.GeoCoordinates.Longitude;
		}

		return azureResource;
	}

	private static Room ConvertPlaceToRoom(Place place)
	{
		var room = new Room
		{
			Address = place.Address,
			AudioDeviceName = EntityExtentions.ExtractFromAdditionalData<string>(place, "audioDeviceName"),
			Building = EntityExtentions.ExtractFromAdditionalData<string>(place, "building"),
			Capacity = EntityExtentions.ExtractFromAdditionalData<int>(place, "capacity"),
			DisplayDeviceName = EntityExtentions.ExtractFromAdditionalData<string>(place, "displayDeviceName"),
			DisplayName = place.DisplayName,
			EmailAddress = EntityExtentions.ExtractFromAdditionalData<string>(place, "emailAddress"),
			FloorLabel = EntityExtentions.ExtractFromAdditionalData<string>(place, "floorLabel"),
			FloorNumber = EntityExtentions.ExtractFromAdditionalData<int>(place, "floorNumber"),
			GeoCoordinates = place.GeoCoordinates,
			Id = place.Id,
			IsWheelChairAccessible = EntityExtentions.ExtractFromAdditionalData<bool>(place, "isWheelChairAccessible"),
			Label = EntityExtentions.ExtractFromAdditionalData<string>(place, "label"),
			Nickname = EntityExtentions.ExtractFromAdditionalData<string>(place, "nickname"),
			Phone = place.Phone,
			VideoDeviceName = EntityExtentions.ExtractFromAdditionalData<string>(place, "videoDeviceName")
		};

		var bookingType = EntityExtentions.ExtractFromAdditionalData<string>(place, "bookingType");
		if (!string.IsNullOrWhiteSpace(bookingType) && Enum.TryParse<BookingType>(bookingType, true, out var enumVal))
		{
			room.BookingType = enumVal;
		}

		return room;
	}
}