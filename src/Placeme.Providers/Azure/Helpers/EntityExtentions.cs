using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text.Json;

using Microsoft.Graph;

namespace Placeme.Providers.Azure.Helpers;

public static class EntityExtentions
{
	public static T? ExtractFromAdditionalData<T, TCollection>(ICollectionPage<TCollection> collectionPage, string key)
	{
		return ExtractFromAdditionalData<T>(collectionPage.AdditionalData, key);
	}

	public static T? ExtractFromAdditionalData<T>(Entity entity, string key)
	{
		return ExtractFromAdditionalData<T>(entity.AdditionalData, key);
	}

	public static T? ExtractFromAdditionalData<T>(IDictionary<string, object> additionalData, string key)
	{
		if (additionalData == null)
		{
			return default;
		}

		if (!additionalData.ContainsKey(key))
		{
			return default;
		}

		var value = additionalData[key];
		if (value == null)
		{
			return default;
		}

		var jsonEl = (JsonElement)value;

		var converter = TypeDescriptor.GetConverter(typeof(T));

		// Note: We have to convert all values to string to be able to convert them correctly!
		return jsonEl.ValueKind switch
		{
			JsonValueKind.False or JsonValueKind.True => (T?)converter.ConvertFrom(jsonEl.GetBoolean().ToString()),
			JsonValueKind.String => (T?)converter.ConvertFrom(jsonEl.GetString()!),
			JsonValueKind.Null => default,
			JsonValueKind.Number => (T?)converter.ConvertFrom(jsonEl.GetInt32().ToString(CultureInfo.InvariantCulture)),
			JsonValueKind.Undefined => throw new NotSupportedException($"Converting JSON value kind {jsonEl.ValueKind} is not yet supported"),
			JsonValueKind.Object => throw new NotSupportedException($"Converting JSON value kind {jsonEl.ValueKind} is not yet supported"),
			JsonValueKind.Array => throw new NotSupportedException($"Converting JSON value kind {jsonEl.ValueKind} is not yet supported"),
			_ => throw new NotSupportedException($"Converting JSON value kind {jsonEl.ValueKind} is not yet supported")
		};
	}
}