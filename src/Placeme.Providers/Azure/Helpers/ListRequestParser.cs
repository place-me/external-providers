
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;

using Microsoft.Graph;

using Placeme.Common.Generated.Base;

using ListRequest = Placeme.Common.Generated.Base.ListRequest;
using Operator = Placeme.Common.Generated.Base.Operator;

namespace Placeme.Providers.Azure.Helpers;

public static class ListRequestExtensions
{
	public static Collection<Option> ConvertToPagingQueryOptions(this ListRequest listRequest)
	{
		var queryOptions = new Collection<Option>()
		{
			new QueryOption("$count", "true")
		};
		if (listRequest.PageSize > 0)
		{
			queryOptions.Add(new QueryOption("$top", listRequest.PageSize.ToString(CultureInfo.InvariantCulture)));
		}
		if (listRequest.PageNumber > 0)
		{
			var skipPages = Math.Max(0, listRequest.PageNumber - 1);
			queryOptions.Add(new QueryOption("$skip", (skipPages * listRequest.PageSize).ToString(CultureInfo.InvariantCulture)));
		}
		return queryOptions;
	}

	public static string ConvertToSortString<T>(this ListRequest listRequest)
	{
		var sort = listRequest.Sort;
		if (sort == null || string.IsNullOrWhiteSpace(sort.SortElement))
		{
			return string.Empty;
		}
		return $"{ConvertSpecialTypes(sort.SortElement, typeof(T))} " + (sort.IsDescending ? "desc" : "asc");
	}

	public static string ConvertToFilterString<T>(this ListRequest listRequest)
	{
		var filterSet = listRequest.FilterSet;
		var filterString = string.Empty;
		if (filterSet?.Filters == null)
		{
			return filterString;
		}
		if (filterSet.Filters.Any(f => f.CompareFields))
		{
			throw new NotSupportedException("Comparing fields is not supported via Graph");
		}
		foreach (var filter in filterSet.Filters)
		{
			if (!string.IsNullOrWhiteSpace(filterString))
			{
				var op = filterSet.LogicalOperator switch
				{
					LogicalOperator.And => "and",
					LogicalOperator.Or => "or",
					_ => "or"
				};
				filterString += $" {op} ";
			}
			filter.Left = ConvertSpecialTypes(filter.Left, typeof(T));
			if (filter.Operator == Operator.Contains)
			{
				filterString += $"{ConvertOperator(filter.Operator, filter.Negate)}({filter.Left},'{filter.Right.Replace("'", "''", StringComparison.InvariantCulture)}')";
			}
			else
			{
				filterString += $"{filter.Left} {ConvertOperator(filter.Operator, filter.Negate)} ";
				filterString += $"'{filter.Right.Replace("'", "''", StringComparison.InvariantCulture)}'";
			}
		}
		return filterString;
	}

	private static string ConvertSpecialTypes(string input, Type t)
	{
		var propertyInfo = t.GetProperty(input, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
		if (propertyInfo == null)
		{
			throw new NotSupportedException("Invalid filter field provided");
		}
		if (propertyInfo.PropertyType == typeof(DateTimeTimeZone))
		{
			return input + "/dateTime";
		}
		return input;
	}

	private static string ConvertOperator(Operator op, bool negate)
	{
		if (negate && op != Operator.Equals)
		{
			throw new NotSupportedException("Negate is only supported with equals");
		}
		return op switch
		{
			Operator.Equals => !negate ? "eq" : "ne",
			Operator.LessThan => "lt",
			Operator.LessThanOrEqual => "le",
			Operator.GreaterThan => "gt",
			Operator.GreaterThanOrEqual => "ge",
			Operator.Contains => "contains",
			_ => "eq"
		};
	}
}