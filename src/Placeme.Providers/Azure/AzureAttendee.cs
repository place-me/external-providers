using Microsoft.Graph;

using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Azure;

public class AzureAttendee : IRemoteAttendee
{
	public string? Email { get; set; }
	public RemoteAttendeeType Type { get; set; }
	public RemoteAttendeeResponseState State { get; set; }
	public string? DisplayName { get; set; }

	public static Attendee Map(IRemoteAttendee attendee)
	{
		return new Attendee
		{
			EmailAddress = new EmailAddress { Address = attendee.Email },
			Type = ToAzureAttendeeType(attendee.Type)
		};
	}

	private static AttendeeType ToAzureAttendeeType(RemoteAttendeeType type)
	{
		return type switch
		{
			RemoteAttendeeType.Optional => AttendeeType.Optional,
			RemoteAttendeeType.Required => AttendeeType.Required,
			RemoteAttendeeType.Resource => AttendeeType.Resource,
			RemoteAttendeeType.Unknown => AttendeeType.Optional,
			_ => AttendeeType.Optional,
		};
	}
}