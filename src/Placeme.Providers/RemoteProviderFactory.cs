
using System.Text.Json;

using Microsoft.Graph;

using Placeme.Providers.Azure;
using Placeme.Providers.Azure.Authentication;
using Placeme.Providers.Interfaces;
using Placeme.Providers.Noop;

namespace Placeme.Providers;

public class RemoteProviderFactory : IRemoteProviderFactory
{
	private static T? DeserializeFromJson<T>(string target)
	{
		return JsonSerializer.Deserialize<T>(target);
	}

	public IRemoteProvider GetAzureProvider(AzureConfiguration? azureConfig, string? accessToken)
	{
		var client = accessToken != null
			? new GraphServiceClient(new TokenAuthProvider(accessToken))
			: new GraphServiceClient(new ClientSecretAuthProvider(azureConfig!));

		return new AzureProvider(client);
	}

	public IRemoteProvider GetNoopProvider()
	{
		return new NoopProvider();
	}

	public IRemoteProvider GetRemoteProvider(RemoteProviderType providerType, string target, string? accessToken)
	{
		switch (providerType)
		{
			case RemoteProviderType.Azure:
				return GetAzureProvider(DeserializeFromJson<AzureConfiguration>(target), accessToken);
			default:
				return GetNoopProvider();
		}
	}
}