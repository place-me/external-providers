using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Placeme.Common.Application.Models;
using Placeme.Common.Generated.Base;
using Placeme.Providers.Azure;
using Placeme.Providers.Interfaces;
using Placeme.Providers.Models;

namespace Placeme.Providers.Noop;

public class NoopProvider : IRemoteProvider
{
	public Task<IRemoteEvent> CreateEventAsync(IRemoteEvent remoteEvent, CancellationToken cancellationToken = default)
	{
		return Task.FromResult<IRemoteEvent>(new AzureEvent());
	}

	public Task DeleteEventAsync(string id, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	public Task<string> GenerateDefaultConfigurationAsync(string configurationJson,
		CancellationToken cancellationToken = default)
	{
		return Task.FromResult("{}");
	}

	public Task<IRemoteEvent> GetEventAsync(string id, CancellationToken cancellationToken = default)
	{
		return Task.FromResult<IRemoteEvent>(new AzureEvent());
	}

	public Task<IEnumerable<RemoteScheduleInformation>> GetFreeBusyAsync(IEnumerable<string> attendees, DateTime start, DateTime end, int interval = 30, CancellationToken cancellationToken = default)
	{
		return Task.FromResult(Array.Empty<RemoteScheduleInformation>().AsEnumerable());
	}

	public Task<IEnumerable<RemoteScheduleInformation>> GetFreeBusyAsync(string userId, IEnumerable<string> attendees, DateTime start, DateTime end,
		int interval = 30, CancellationToken cancellationToken = default)
	{
		return Task.FromResult(Array.Empty<RemoteScheduleInformation>().AsEnumerable());
	}

	public Task<IEnumerable<RemoteAsset>> GetAssetsAsync(CancellationToken cancellationToken = default)
	{
		return Task.FromResult(new List<RemoteAsset>().AsEnumerable());
	}

	public Task<IEnumerable<IRemoteUser>> GetUsersAsync(CancellationToken cancellationToken = default)
	{
		return Task.FromResult(new List<IRemoteUser>().AsEnumerable());
	}

	public Task<PaginatedList<IRemoteEvent>> ListEventsAsync(ListRequest listRequest, CancellationToken cancellationToken = default)
	{
		return Task.FromResult(new PaginatedList<IRemoteEvent>(new List<IRemoteEvent>(), 0, 0, 0));
	}

	public Task<IEnumerable<IRemotePermission>> ReviewPermissionsAsync(CancellationToken cancellationToken = default)
	{
		return Task.FromResult(new List<IRemotePermission>().AsEnumerable());
	}

	public Task<IRemoteEvent> UpdateEventAsync(IRemoteEvent remoteEvent, RemoteEventUpdatableProperties propertiesToUpdate, CancellationToken cancellationToken = default)
	{
		return Task.FromResult<IRemoteEvent>(new AzureEvent());
	}

	public Task<bool> ValidateConfigurationAsync(string configurationJson,
		CancellationToken cancellationToken = default)
	{
		return Task.FromResult(true);
	}

	public Task<byte[]> GetPhotoAsync(string id, CancellationToken cancellationToken = default)
	{
		return Task.FromResult(Array.Empty<byte>());
	}
}