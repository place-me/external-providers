using System.Collections.Generic;

namespace Placeme.Providers.Interfaces;

public interface IRemoteUser
{
	string? City { get; set; }
	string? Country { get; set; }
	string? Department { get; set; }
	string? Division { get; set; }
	string? Email { get; set; }
	string? Firstname { get; set; }
	IEnumerable<IRemoteUserGroup> Groups { get; set; }
	string? Id { get; set; }
	string? Lastname { get; set; }
	string? PhoneNumber { get; set; }
	string? Function { get; set; }
	string? Street { get; set; }
	string? ZipCode { get; set; }
}