namespace Placeme.Providers.Interfaces;

public interface IRemoteUserGroup
{
	string? Id { get; set; }
	string? Description { get; set; }
	string? Name { get; set; }
}