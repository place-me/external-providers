using System;
using System.Collections.Generic;

using Placeme.Providers.Models;

namespace Placeme.Providers.Interfaces;

public interface IRemoteEvent
{
	string? Id { get; set; }
	string? Subject { get; set; }
	string? Body { get; set; }
	string? BodyPreview { get; set; }
	DateTime Start { get; set; }
	DateTime End { get; set; }
	IEnumerable<IRemoteAttendee> Attendees { get; set; }
	RemoteRecurrencePattern? Recurrence { get; set; }
	public IEnumerable<string> Categories { get; set; }
	public RemoteSensitivity Sensitivity { get; set; }
	public RemoteFreeBusyStatus FreeBusyStatus { get; set; }
	public bool? HasAttachments { get; set; }
	public bool? IsOnlineMeeting { get; set; }
	int? ReminderMinutesBeforeStart { get; set; }
	bool? IsReminderOn { get; set; }
	IRemoteAttendee? Organizer { get; set; }
	public bool? IsOrganizer { get; set; }
}