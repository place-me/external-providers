namespace Placeme.Providers.Interfaces;

public enum RemoteAttendeeResponseState
{
	None = 0,
	Organizer = 1,
	TentativelyAccepted = 2,
	Accepted = 3,
	Declined = 4,
	NotResponded = 5
}