namespace Placeme.Providers.Interfaces;

public enum RemoteFreeBusyStatus
{
	Unknown = -1,

	Free = 0,

	Tentative = 1,

	Busy = 2,

	OutOfOffice = 3,

	WorkingElsewhere = 4
}