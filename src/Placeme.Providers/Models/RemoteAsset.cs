namespace Placeme.Providers.Models;

public class RemoteAsset
{
	public string? AudioDeviceName { get; set; }
	public string? BookingType { get; set; }
	public string? Building { get; set; }
	public int? Capacity { get; set; }
	public string? City { get; set; }
	public string? Country { get; set; }
	public string? DisplayDeviceName { get; set; }
	public string? Email { get; set; }
	public int? FloorNumber { get; set; }
	public string? FloorLabel { get; set; }
	public double? GeoCoordinatesAltitude { get; set; }
	public double? GeoCoordinatesLongitude { get; set; }
	public double? GeoCoordinatesLatitude { get; set; }
	public string? Id { get; set; }
	public bool? IsWheelChairAccessible { get; set; }
	public string? Label { get; set; }
	public string? Name { get; set; }
	public string? Nickname { get; set; }
	public string? Phone { get; set; }
	public string? RemoteConnectionId { get; set; }
	public string? State { get; set; }
	public string? Street { get; set; }
	public string? VideoDeviceName { get; set; }
	public string? ZipCode { get; set; }
	public string? Upn { get; set; }
}