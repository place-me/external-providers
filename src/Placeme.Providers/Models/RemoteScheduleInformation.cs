using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Placeme.Providers.Models;

public class RemoteScheduleInformation
{
	/// <summary>
	/// Represents a merged view of availability of all the items in scheduleItems.
	/// The view consists of time slots.
	/// Availability during each time slot is indicated with:
	/// 0= free, 1= tentative, 2= busy, 3= out of office, 4= working elsewhere.
	/// </summary>
	public string AvailabilityView { get; set; } = string.Empty;

	/// <summary>
	/// Error information from attempting to get the availability of the user, distribution list, or resource.
	/// </summary>
	public RemoteFreeBusyError? Error { get; set; }

	/// <summary>
	///	An identifier of the user, distribution list, or resource, identifying an instance of scheduleInformation.
	/// </summary>
	public string ScheduleId { get; set; } = string.Empty;

	public ICollection<RemoteScheduleItem> ScheduleItems { get; } = new Collection<RemoteScheduleItem>();
}