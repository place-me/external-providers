using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Placeme.Providers.Models;

public class RemoteRecurrencePattern
{
	public int? Interval { get; set; }
	public RemoteRecurrenceType? Type { get; set; }
	public IEnumerable<RemoteRecurrenceWeekDay> DaysOfWeek { get; set; }
	public int? DayOfMonth { get; set; }
	public RemoteRecurrenceWeekIndex? Index { get; set; }
	public int? Month { get; set; }
	public RemoteRecurrenceRange Range { get; set; }

	public RemoteRecurrencePattern()
	{
		Range = new RemoteRecurrenceRange();
		DaysOfWeek = new Collection<RemoteRecurrenceWeekDay>();
	}
}